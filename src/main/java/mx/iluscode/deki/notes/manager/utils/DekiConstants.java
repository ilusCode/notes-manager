package mx.iluscode.deki.notes.manager.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/***************************************
 *  @description
 *
 *  @project deki
 *  @author Team GFT Dev BaaS
 *  @created 09/12/21 - 23:54
 *
 ***************************************/
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DekiConstants {

    /**
     * Constant PARAMETER_IMAGE
     */
    public static final String PARAMETER_IMAGE = "Image";

    /**
     * Constant FORMAT_FOUR_ZEROS
     */
    public static final String FORMAT_FOUR_ZEROS = "%04d";

    /**
     * Constant SEPARADOR
     */
    public static final String SEPARADOR = "-------------------------------------------------";
}
