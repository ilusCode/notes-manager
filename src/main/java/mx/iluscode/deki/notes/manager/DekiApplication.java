package mx.iluscode.deki.notes.manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class DekiApplication {

    public static void main(String[] args) {
        SpringApplication.run(DekiApplication.class, args);
    }

}
